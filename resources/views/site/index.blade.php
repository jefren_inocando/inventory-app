@extends('layouts.master')

@section('title')
    Skills Test
@endsection

@section('scripts')
    <script>
        $(function () {

            // post new product item
            $("#productForm").submit(function (e) {
                $.post("{{ route('site.create') }}", $(this).serialize()).done(function (data) {
                    $("#display").html(data);
                });
                e.preventDefault();
            });

            // loads edit form and values
            $("#myModal").on("show.bs.modal", function (e) {
                $("#modalBody").empty();
                var prodId = $(e.relatedTarget).data("count");
                $.get("{{ route('site.edit') }}" + "?id=" + prodId, function (data) {
                    $("#modalBody").html(data);
                });
            });

            // saves edit values
            $("#modalBtn").click(function () {
                $.ajax({
                    url: "{{ route('site.edit') }}",
                    type: 'PUT',
                    data: $("#editForm").serialize(),
                    success: function (result) {
                        $("#display").html(result);
                        $('#myModal').modal('hide');
                    }
                });
            });

        });
    </script>
@endsection

@section('content')
    <div class="row">
        <form id="productForm" method="POST">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" required="required">
            </div>

            <div class="form-group">
                <label for="quantity_in_stock">Quantity In Stock</label>
                <input type="number" class="form-control" id="quantity_in_stock" name="quantity_in_stock"
                       placeholder="Quantity In Stock" required="required">
            </div>

            <div class="form-group">
                <label for="price_per_item">Price per item</label>
                <input type="number" class="form-control" id="price_per_item" name="price_per_item"
                       placeholder="Price per item" required="required">
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>

    <div class="row" id="display">
        @include('site._productDisplay',['products'=>$products])
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Product</h4>
                </div>
                <div class="modal-body" id="modalBody"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="modalBtn">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection