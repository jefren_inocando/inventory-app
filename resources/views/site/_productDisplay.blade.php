<table class="table" style="margin-top: 20px;">
    <thead>
    <tr>
        <th>Product Name</th>
        <th>Quantity in stock</th>
        <th>Price per item</th>
        <th>Datetime submitted</th>
        <th>Total value number</th>
        <th></th>
    </tr>
    </thead>

    <tbody>
    <?php
    $total = 0;
    $count = 0;
    foreach($products as $product):
    $total += $product->price_per_item * $product->quantity_in_stock;
    ?>
    <tr>
        <td>{{ $product->name }}</td>
        <td>{{ $product->quantity_in_stock }}</td>
        <td>{{ $product->price_per_item }}</td>
        <td>{{ $product->created_at }}</td>
        <td>{{ $product->price_per_item * $product->quantity_in_stock}}</td>
        <td>
            <a href="" data-toggle="modal" data-target="#myModal" data-count="{{ $count }}">
                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            </a>
        </td>
    </tr>
    <?php
    $count++;
    endforeach;
    ?>
    </tbody>

</table>

<h1 class="pull-right">Total: {{ $total }}</h1>