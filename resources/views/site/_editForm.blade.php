<form id="editForm" method="PUT">

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $product->name }}"
               required="required"/>
    </div>

    <div class="form-group">
        <label for="quantity_in_stock">Quantity In Stock</label>
        <input type="number" class="form-control" id="quantity_in_stock" name="quantity_in_stock"
               placeholder="Quantity In Stock" value="{{ $product->quantity_in_stock }}" required="required"/>
    </div>

    <div class="form-group">
        <label for="price_per_item">Price per item</label>
        <input type="number" class="form-control" id="price_per_item" name="price_per_item" placeholder="Price per item"
               value="{{ $product->price_per_item }}" required="required"/>
    </div>

    <input type="hidden" name="created_at" value="{{ $product->created_at }}">

    <input type="hidden" name="id" value="{{ $id }}">
</form>