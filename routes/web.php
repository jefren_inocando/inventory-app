<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [
    'uses' => 'SiteController@getIndex',
    'as' => 'home'
]);

Route::post('/', [
    'uses' => 'SiteController@postIndex',
    'as' => 'site.create'
]);

Route::get('/edit', [
    'uses' => 'SiteController@getEditForm',
    'as' => 'site.edit'
]);

Route::put('/edit', [
    'uses' => 'SiteController@putEditForm',
    'as' => 'site.edit'
]);