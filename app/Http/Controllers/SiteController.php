<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{

    /**
     * Display forms and tables for products
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getIndex(Request $request)
    {
        $products = $this->getSortedProductJson();
        return view('site.index', [
            'products' => $products,
        ]);
    }

    /**
     * Saves new product item to json file and returns sorted products in html table
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function postIndex(Request $request)
    {
        // get and decode json file
        $products = json_decode(file_get_contents(public_path() . '/products.json'));

        // save new product item
        $data = $request->input();
        $data['created_at'] = date('Y-m-d H:i:s');
        $products[] = $data;

        // save new product to json file
        file_put_contents(public_path() . '/products.json', json_encode($products));

        return view('site._productDisplay', [
            'products' => $this->getSortedProductJson(),
        ]);
    }

    /**
     * Load form and values for edit form
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getEditForm(Request $request)
    {
        $products = $this->getSortedProductJson();
        return view('site._editForm', [
            'product' => $products[$request->input('id')],
            'id' => $request->input('id')
        ]);
    }

    /**
     * Saves edit values
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function putEditForm(Request $request)
    {
        // saves new values
        $products = $this->getSortedProductJson();
        $products[$request->input('id')] = [
            'name' => $request->input('name'),
            'quantity_in_stock' => $request->input('quantity_in_stock'),
            'price_per_item' => $request->input('price_per_item'),
            'created_at' => $request->input('created_at'),
        ];

        // save new product to json file
        file_put_contents(public_path() . '/products.json', json_encode($products));

        return view('site._productDisplay', [
            'products' => $this->getSortedProductJson(),
        ]);
    }

    /**
     * Gets sorted products from json file
     * @return mixed
     */
    public function getSortedProductJson()
    {
        // get and decode json file
        $products = json_decode(file_get_contents(public_path() . '/products.json'));

        // order by date time submitted
        usort($products, function ($a, $b) {
            return strtotime($a->created_at) - strtotime($b->created_at);
        });

        return $products;
    }
}
