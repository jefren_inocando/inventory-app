<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';
    protected $guarded = ['id'];

    public function getTotalPriceAttribute() {
        return $this->quantity_in_stock * $this->price_per_item;
    }
}
